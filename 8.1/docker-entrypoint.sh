#!/bin/sh

set -e

auto_envsubst() {
    [[ -z "$NGINX_ALLOW_METHOD" ]] && export NGINX_ALLOW_METHOD="GET|HEAD|POST" || export NGINX_ALLOW_METHOD="${NGINX_ALLOW_METHOD//, /|}"
    [[ -z "$NGINX_STATS_ALLOW_IP" ]] && export NGINX_STATS_ALLOW_IP="" || export NGINX_STATS_ALLOW_IP="allow $NGINX_STATS_ALLOW_IP;"
    [[ -z "$NGINX_CSP_RULE" ]] && export NGINX_CSP_RULE="" || export NGINX_CSP_RULE="add_header Content-Security-Policy \"$NGINX_CSP_RULE\" always;"
    [[ -z "$PHP_INI_VARS" ]] && export PHP_INI_VARS="" || sed -i "/# This is custom PHP/a \    fastcgi_param PHP_ADMIN_VALUE \"${PHP_INI_VARS//,/ '\\n' }\";" /etc/nginx/http.d/default.conf.template
        
    DEFINE_VARS=$(printf '${%s}' $(env | cut -d= -f1))
    TEMPLATE_DIR="/etc/nginx/http.d"
    TEMPLATE_SUFFIX=".template"
    OUTPUT_DIR="/etc/nginx/http.d"

    find "$TEMPLATE_DIR" -follow -type f -name "*$TEMPLATE_SUFFIX" -print | while read -r template; do
        RELATIVE_PATH="${template#$TEMPLATE_DIR/}"
        OUTPUT_PATH="$OUTPUT_DIR/${RELATIVE_PATH%$TEMPLATE_SUFFIX}"
        echo "Running envsubst on $template to $OUTPUT_PATH"
        envsubst "$DEFINE_VARS" < "$template" > $OUTPUT_PATH
        rm -f $template
    done
}

auto_envsubst

exec "$@"
