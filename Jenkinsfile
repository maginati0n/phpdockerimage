pipeline{
    agent any
    options{
        skipDefaultCheckout true
        skipStagesAfterUnstable()
        disableConcurrentBuilds()
        buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '30', daysToKeepStr: '', numToKeepStr: '30')
    }
    triggers{
        bitbucketPush overrideUrl: ''
        bitBucketTrigger([
            [
                $class: 'BitBucketPPRPullRequestTriggerFilter',
                actionFilter: [
                    $class: 'BitBucketPPRPullRequestCreatedActionFilter'
                ]
            ],
            [
                $class: 'BitBucketPPRRepositoryTriggerFilter',
                actionFilter: [
                    $class: 'BitBucketPPRRepositoryPushActionFilter',
                    allowedBranches: '',
                    triggerAlsoIfTAGPush: true
                ]
            ]
        ])
    }
    environment{
        PROJECT = "php-docker"
        REGISTRY = "rollingglory/php-nginx"
        REGISTRY_CREDENTIAL = "docker-hub-rollingglory"
    }
    stages{
        stage("Checkout"){
            steps{
                checkout scm
            }
        }
        stage("Build Image"){
            steps{
                script{
                    FOLDER_LIST = sh(returnStdout: true, script: "ls -d */ | sed 's:/*\$::'")
                    FOLDER_LIST = FOLDER_LIST.split('\n')

                    for(String PHP in FOLDER_LIST) {
                        sh label: "Build PHP ${PHP} Docker Image", script: "docker image build -t ${REGISTRY}:${PHP} -f ${PHP}/Dockerfile ${PHP}"
                    }
                }
            }
        }
        stage("Test Image"){
            steps{
                script{
                    for(String PHP in FOLDER_LIST) {
                        DOCKER_EXIT_CODE = 0
                        
                        sh label: "Run container", script: "docker run --name ${PROJECT}-${PHP}-${env.BUILD_NUMBER} -d ${REGISTRY}:${PHP}"
                        sleep 10
                        
                        DOCKER_EXIT_CODE = sh(returnStdout: true, script: "docker inspect ${PROJECT}-${PHP}-${env.BUILD_NUMBER} ${PROJECT}-${PHP}-${env.BUILD_NUMBER} -f '{{ .State.ExitCode }}'").trim()
                        sh label: 'Get container logs', script: "docker logs ${PROJECT}-${PHP}-${env.BUILD_NUMBER}"
                        sh label: "Force stop container", script: "docker rm -f ${PROJECT}-${PHP}-${env.BUILD_NUMBER}"
                    }
                }
            }
        }
        stage("Push Image"){
            steps{
                script{
                    docker.withRegistry('', REGISTRY_CREDENTIAL) {
                        for(String PHP in FOLDER_LIST) {
                            sh label: 'Push PHP ${PHP} Docker Image', script: "docker image push ${REGISTRY}:${PHP}"
                        }
                    }
                }
            }
        }
    }
}
