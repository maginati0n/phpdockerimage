FROM php:7.2-fpm-alpine
LABEL name="PHP 7.2 FPM - NGINX Base Image" \
    version="1.0" \
    description="Container image with PHP 7.1 and NGINX 1.22"

# NGINX env variable
ENV TZ Asia/Jakarta
ENV NGINX_PORT 8080
ENV NGINX_STATUS_PORT 32574
ENV NGINX_DOMAIN localhost
ENV DOCUMENT_ROOT /var/www/html
ARG UID=1000
ARG GID=1000
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so

# Install PHP extension and NGINX web server
RUN set -eux; \
        addgroup -g ${GID} -S nginx; \
        adduser -S -D -H -u ${UID} -h /var/cache/nginx -s /sbin/nologin -G nginx -g nginx nginx; \
        apk add --update --no-cache --virtual .build-dependencies \
            autoconf \
            g++ \
            icu-dev \
            freetype-dev \
            libjpeg-turbo-dev \
            libpng-dev \
            libwebp-dev \
            libzip-dev \
            gmp-dev \
            make; \
        apk --update --no-cache add \
            ca-certificates \
            freetype \
            gettext \
            gifsicle \
            icu-libs \
            jpegoptim \
            libintl \
            libjpeg-turbo \
            libpng \
            libzip \
            libwebp \
            nginx \
            optipng \
            pngquant \
            supervisor \
            tzdata \
            gnu-libiconv \
            gmp \
            zip; \
        pecl install redis; \
        docker-php-ext-enable redis; \
        docker-php-ext-configure gd --with-gd --with-webp-dir=/usr/include/ --with-freetype-dir=/usr/include/ --with-png-dir=/usr/include/ --with-jpeg-dir=/usr/include/; \
        docker-php-ext-install -j$(nproc) \
            gd \
            gmp \
            mysqli \
            pdo_mysql \
            opcache \
            zip \
            iconv \
            intl; \
        apk del --no-network .build-dependencies; \
        rm -fr /var/cache/apk/*; \
        rm -rf /tmp/pear ~/.pearrc

# Configure NGINX and PHP-FPM
RUN set -eux; \
        # Forward nginx log to docker stdout
        ln -sf /dev/stdout /var/log/nginx/access.log; \
        ln -sf /dev/stderr /var/log/nginx/error.log; \
        # Implement configuration for NGINX unprivileged user
        sed -i '/user  nginx;/d' /etc/nginx/nginx.conf; \
        sed -i 's,/var/run/nginx.pid,/tmp/nginx.pid,' /etc/nginx/nginx.conf; \
        sed -i "/^http {/a \    proxy_temp_path /tmp/proxy_temp;\n    client_body_temp_path /tmp/client_temp;\n    fastcgi_temp_path /tmp/fastcgi_temp;\n    uwsgi_temp_path /tmp/uwsgi_temp;\n    scgi_temp_path /tmp/scgi_temp;\n" /etc/nginx/nginx.conf; \
        # Tuning NGINX web server
        sed -i 's,client_max_body_size 1m;,client_max_body_size 25m;,' /etc/nginx/nginx.conf; \
        sed -i 's,#gzip on;,gzip on;,' /etc/nginx/nginx.conf; \
        # Configure security from PHP ini
        mv $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini; \
        sed -i 's/expose_php = On/expose_php = Off/g' $PHP_INI_DIR/php.ini; \
        sed -i 's/disable_functions =/disable_functions = exec,passthru,shell_exec,system/g' $PHP_INI_DIR/php.ini; \
        # Adjust directory permission for NGINX unprivileged user
        rm /etc/nginx/conf.d/default.conf; \
        mkdir -p /var/cache/nginx; \
        chown -R nginx:0 /var/cache/nginx; \
        chmod -R g+w /var/cache/nginx; \
        chown -R nginx:0 /etc/nginx; \
        chmod -R g+w /etc/nginx

# Copy Supervisord, NGINX and PHP configuration
COPY config/*.template /etc/nginx/conf.d/
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY config/www.conf /usr/local/etc/php-fpm.d/www.conf
COPY --chown=nginx:nginx docker-entrypoint.sh /

# Make document root folder
RUN mkdir -p /var/www/html && \
    chown -R nginx:nginx /var/www/html && \
    chmod +x /docker-entrypoint.sh
COPY --chown=nginx:nginx src /var/www/html/

ENTRYPOINT [ "/docker-entrypoint.sh" ]

STOPSIGNAL SIGQUIT

EXPOSE 8080 $NGINX_STATUS_PORT

USER nginx

CMD [ "/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/conf.d/supervisord.conf" ]
