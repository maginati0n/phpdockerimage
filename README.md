# About this image
This is a PHP container image with NGINX web server for running containerize PHP application

# Benefit use this image
- Use [Official PHP container image](https://hub.docker.com/_/php) Alpine Linux as base image
- Small container image size
- This image use unprivileged user to run container
- Some NGINX and PHP config can be adjust via environment variable

# How to use this image
## Running container
```bash
$ docker run --name my-php -p 8080:8080 rollingglory/php-nginx:8.1
```
To access container, open browser and access `http://localhost:8080` or`http://host-ip:8080`. Default page of this container image only show PHP information

## Running container with simple PHP website
```bash
$ docker run --name my-web -v /path/to/your/website:/var/www/html -p 8080:8080 rollingglory/php-nginx:8.1
```
Or you can create simple `Dockerfile` to create new container image with your PHP application
```
FROM rollingglory/php-nginx:8.1
COPY php-website /var/www/html
```
Place `Dockerfile` folder in the same directory as your php web directory (e.g. php-website) and build container image using command `docker image build -t my-php-web .` and start the container
```bash
docker run --name my-php-web -p 8080:8080 my-php-web
```

## Using environment variables to run container
Here is an example using docker-compose.yml with environment variables:
```
my-php-web:
  image: rollingglory/php-nginx:8.1
  volumes:
   - ./php-website:/var/www/html
  ports:
   - 8080:8080
  environment:
   - DOCUMENT_ROOT=/var/www/html
   - NGINX_DOMAIN=myphpwebsite.com
   - NGINX_PORT=8080
```

Available environment variables that can be changed:

- `DOCUMENT_ROOT` - Define document root php application. Default `/var/www/html`

- `NGINX_DOMAIN` - Define domain name for NGINX web server. Default `localhost`

- `NGINX_PORT` - Define port number NGINX web server. Default `8080`

- `NGINX_STATUS_PORT` - Define port number for access NGINX and PHP status page. Default `32574`

- `NGINX_CSP_RULE` - Define content security policy for NGINX web server. Default `""`. (e.g. `NGINX_CSP_RULE="default-src 'self' data: blob:"`)

- `NGINX_ALLOW_METHOD` - Limit NGINX HTTP method (comma separated). Default `GET,HEAD,POST`

- `NGINX_STATS_ALLOW_IP` - Allow certain IP address to access NGINX and PHP statistic. Default `127.0.0.1`

- `PHP_INI_VARS` - Define custom PHP INI settings (comma separated). Default `""`. (e.g. `PHP_INI_VARS="memory_limit=256M,post_max_size=10M"`)
